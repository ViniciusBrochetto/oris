﻿using UnityEngine;
using System.Collections;

public interface IInteractable {
    void Interact();
    bool GetInteractable();
}
